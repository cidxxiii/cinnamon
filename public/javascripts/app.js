angular.module("Cinnamon", [
  "ngRoute",
  "ngCookies",
  "Services"
]).config(function ($routeProvider) {
  $routeProvider.when("/", {
    templateUrl: "views/main.html",
    controller: "MainCtrl"
  }).otherwise({
    redirectTo: "/"
  });
});

angular.module("Cinnamon").
  controller("MainCtrl", function ($rootScope, $scope, $cookies, $timeout, server) {
  /**
   * Watch for session.
   */
  $scope.$watch(function () { return $cookies.session; }, function (newValue, oldValue) {
    $scope.session = newValue;
  });

  /**
   * Watch for error messages.
   */
  $scope.close = function () {
    delete $rootScope.flash;
  };

  /**
   * onClick events for buttons.
   */
  $scope.workspace = function () {
    $scope.links = server.workspace();
  };

  $scope.get = function (format, url) {
    $scope.links = server.get(format, url);
  };

  $scope.clean = function () {
    server.clean();
    delete $scope.links;
  };

  /**
   * Controller initialization.
   */
  $scope.init = function () {
    $timeout(function () {
      $scope.formats = server.formats();
      $scope.links = server.workspace();
    }, 1000);
  };
});

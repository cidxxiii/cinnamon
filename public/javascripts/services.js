angular.module("Services", [
  "ngCookies"
]).factory("socket", [
  function () {
    var socket = { session: null };

    ab.connect("wss://sonnenkarma.de:24024", function (newSession) {
      socket.session = newSession;
      console.debug("Verbindung zu 'sonnenkarma.de' hergestellt", newSession.sessionid());
    }, function (code, reason, detail) {
      console.debug("Verbindung zum Server kann nicht hergestellt werden!", code, reason, detail);
    });

    return socket;
  }
]).factory("server", [
  "$rootScope",
  "$q",
  "$cookies",
  "socket",
  function ($rootScope, $q, $cookies, socket) {
    var server = {};

    server.workspace = function () {
      if (!socket.session) {
        console.debug("[server.workspace] no websocket connection");
        return;
      }

      var defer = $q.defer();
      socket.session.call("cinnamon:create", $cookies.session).then(function (res) {
        $cookies.session = res;

        socket.session.call("cinnamon:workspace", $cookies.session).then(function (res) {
          defer.resolve(JSON.parse(res));
          console.debug("retrieved workspace", res);
        }, function (err) {
          console.debug("[server.workspace]", err);
        });
      }, function (err) {
        console.debug("[server.workspace]", err);
      });
      return defer.promise;
    };

    server.get = function (format, url) {
      if (!socket.session) {
        console.debug("[server.get] no websocket connection");
        return;
      }

      var defer = $q.defer();
      socket.session.call("cinnamon:create", $cookies.session).then(function (res) {
        $cookies.session = res;

        $rootScope.$apply("loadingIndicator = true");
        socket.session.call("cinnamon:get", $cookies.session, format, url).then(function (res) {
          $rootScope.$apply("loadingIndicator = false");

          socket.session.call("cinnamon:workspace", $cookies.session).then(function (res) {
            defer.resolve(JSON.parse(res));
            console.debug("retrieved workspace", res);
          }, function (err) {
            console.debug("[server.get]", err);
          });
        }, function (err) {
          console.debug("[server.get]", err);
          $rootScope.$apply("loadingIndicator = false");
          $rootScope.$apply("flash = '" + err.desc + "'");
        });
      }, function (err) {
        console.debug("[server.get]", err);
      });
      return defer.promise;
    };

    server.formats = function () {
      if (!socket.session) {
        console.debug("[server.formats] no websocket connection");
        return;
      }

      var defer = $q.defer();
      socket.session.call("cinnamon:formats").then(function (res) {
        console.debug("supported output formats", res);
        defer.resolve(JSON.parse(res));
      }, function (err) {
        console.debug("[server.formats]", err);
      });
      return defer.promise;
    };

    server.clean = function () {
      if (!socket.session) {
        console.debug("[server.clean] no websocket connection");
        return;
      }

      socket.session.call("cinnamon:clean", $cookies.session).then(function (res) {
        delete $cookies.session;
        console.debug("session deleted");
      }, function (err) {
        console.debug("[server.clean]", err);
      });
      return;
    };

    return server;
  }
]);

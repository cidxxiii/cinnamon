var debug = require("debug")("server"),
  __ = require("underscore"),
  fs = require("fs"),
  https = require("https"),
  WebSocketServer = require("ws").Server,
  wamp = require("wamp.io"),
  uuid = require("node-uuid"),
  exec = require("child_process").exec,
  argv = require("optimist").argv;
  //redis = require("redis");

/**
 * Load config file
 */
var config = require(argv.config);

/**
 * Create the https server.
 */
var opts = {
  cert: fs.readFileSync(config.https.cert),
  key: fs.readFileSync(config.https.key)
};
if (config.https.hasOwnProperty("ca")) {
  opts.ca = fs.readFileSync(config.https.ca);
}
var httpsServer = https.createServer(opts);
httpsServer.listen(config.port);
debug("https server listen on port", config.port);

/**
 * Create the websocket server and attach the WAMP protocol.
 */
var wss = new WebSocketServer({ server: httpsServer });

var wamps = wamp.attach(wss);

/**
 * Create the database connection.
 */
/*
var db = redis.createClient(
  process.env.REDIS_PORT || 6379,
  process.env.REDIS_HOST || "localhost",
  {}
);

db.on("connect", function () {
  debug();
  // TODO init models
  debug("redis datastore connected");
});

db.on("error", function (err) {
  debug("cannot connect to redis datastore", err);
  process.exit(1);
});
*/

/**
 * The remote procedures.
 */
var procs = {
  "test:hello": function (args, callback) {
    callback(null, "Hello " + args.shift() + "!");
  },
  "cinnamon:formats": function (args, callback) {
    var formats = [];

    var key;
    for (key in config.ffmpeg.outputFileFormats) {
      if (config.ffmpeg.outputFileFormats.hasOwnProperty(key)) {
        formats.push(key);
      }
    }

    if (formats.length === 0) {
      debug("no formats available");
      return callback("Es sind keine Ausgabeformate definiert! Bitte wende dich an Cid.");
    }
    debug("valid target formats", formats);
    return callback(null, JSON.stringify(formats));
  },
  "cinnamon:create": function (args, callback) {
    var sessionId = args.shift() || uuid.v4();
    debug("created session", sessionId);
    return callback(null, sessionId);
  },
  "cinnamon:get": function (args, callback) {
    var id = args.shift(),
      format = args.shift(),
      url = args.shift(),
      //workingDir = __dirname + "/public/workspaces/" + id,
      workingDir = config.publicDir + "/workspaces/" + id,
      inputDir = workingDir + "/input";

    if (!config.ffmpeg.outputFileFormats.hasOwnProperty(format)) {
      debug("invalid target format", format);
      return callback("Das angeforderte Zielformat ist nicht definiert! Bitte wende dich an Cid.");
    }

    function run () {
      var cmd = config.quvi.replace(/%url%/ig, url);
      debug("executing command", cmd);
      exec(cmd, { cwd: inputDir }, function (err, stdout, stderr) {
        if (err) {
          debug("cannot download url", url, err);
          return callback("Es trat ein Fehler beim Herunterladen auf! Bitte überprüfe, ob es sich bei deinem Link um ein Video oder eine NICHT private Playlist handelt.");
        }
        debug("finished download", url, stdout, stderr);

        fs.readdir(inputDir, function (err, files) {
          if (err) {
            debug("cannot read working directory", err);
            return callback("Es trat ein Fehler beim Lesen des Arbeitsverzeichnisses auf! Bitte wende dich an Cid.");
          }

          var finished = 0;
          __.each(files, function (file) {
            var regex = new RegExp(config.ffmpeg.inputFileFormat.regex, config.ffmpeg.inputFileFormat.modifier);

            function checkFinished () {
              if (finished === files.length) {
                debug("finished converting", workingDir);
                return callback(null, "Download und Konvertierung abgeschlossen");
              }
            }

            if (regex.test(file)) {
              var outfile = file.substring(0, file.lastIndexOf("."));
              var cmd = config.ffmpeg.outputFileFormats[format].
                replace(/%infile%/ig, file).
                replace(/%outfile%/ig, outfile);

              debug("executing command", cmd);
              exec(cmd, { cwd: inputDir }, function (err, stdout, stderr) {
                if (err) {
                  debug("cannot convert file", err, stdout, stderr);
                  return callback("Es trat ein Fehler beim Erstellen der Datei '" + outfile + "' auf! Bitte wende dich an Cid.");
                }

                debug("waiting for finish", files.length - finished, "more");
                finished++;
                checkFinished();
              });
            } else {
              debug("unsupported target format", file);
              finished++;
              checkFinished();
            }
          });
        });
      });
    }

    fs.exists(workingDir, function (exists) {
      if (exists) {
        debug("working directory exists", workingDir);
        return run();
      }

      fs.mkdir(workingDir, "0755", function (err) {
        if (err) {
          debug("cannot create working directory");
          return callback("Es konnte kein Arbeitsverzeichnis erzeugt werden! Bitte wende dich an Cid.");
        }

        fs.mkdir(workingDir + "/input", "0755", function (err) {
          if (err) {
            debug("cannot create working directory (input)");
            return callback("Es konnte kein Eingangs-Arbeitsverzeichnis erzeugt werden! Bitte wende dich an Cid.");
          }

          fs.mkdir(workingDir + "/output", "0755", function (err) {
            if (err) {
              debug("cannot create working directory (output)");
              return callback("Es konnte kein Ausgabe-Arbeitsverzeichnis erzeugt werden! Bitte wende dich an Cid.");
            }

            debug("working directory created", workingDir);
            return run();
          });
        });
      });
    });
  },
  "cinnamon:workspace": function (args, callback) {
    var sessionId = args.shift(),
      //workingDir = __dirname + "/public/workspaces/" + sessionId;
      workingDir = config.publicDir + "/workspaces/" + sessionId;

    fs.exists(workingDir, function (exists) {
      if (!exists) {
        debug("workspace doesn't exists", workingDir);
        return callback("Das aufgerufene Arbeitsverzeichnis existiert nicht!");
      }

      fs.readdir(workingDir + "/output", function (err, files) {
        if (err) {
          debug("error reading workspace", workingDir);
          return callback("Das Arbeitsverzeichnis kann nicht gelesen werden!");
        }

        var links = [];
        __.each(files, function (file) {
          links.push([file, "workspaces/" + sessionId + "/output/" + file]);
        });

        debug("sending workspace", workingDir);
        return callback(null, JSON.stringify(links));
      });
    });
  },
  "cinnamon:clean": function (args, callback) {
    var sessionId = args.shift(),
      //workingDir = __dirname + "/public/workspaces/" + sessionId;
      workingDir = config.publicDir + "/workspaces/" + sessionId;

    fs.exists(workingDir, function (exists) {
      if (!exists) {
        debug("no workspace to clean up", workingDir);
        return callback("Das aufzuräumende Arbeitsverzeichnis existiert nicht (mehr)!");
      }

      function unlinkFiles(directory, files) {
        var count = 0;
        __.each(files, function (file) {
          fs.unlink(directory + "/" + file, function (err) {
            if (err) {
              debug("cannot unlink file", file);
              onFinished("Eine Datei aus dem Arbeitsverzeichnis konnte nicht entfernt werden! Bitte wende dich an Cid.");
            }

            count++;
            if (count === files.length) {
              fs.rmdir(directory, function (err) {
                if (err) {
                  debug("cannot clean up workspace", directory);
                  return onFinished("Das Arbeitsverzeichnis konnte nicht gelöscht werden! Bitte wende dich an Cid.");
                }

                debug("cleaned up workspace", directory);
                return onFinished(null, "Das Arbeitsverzeichnis wurde bereinigt.");
              });
            }
          });
        });
      }

      var finishedCount = 0;
      function onFinished(err, message) {
        if (err) {
          debug(err);
          return callback(err);
        }

        finishedCount++;
        if (finishedCount === 2) {
          debug(message);
          return callback(null, message);
        }
      }

      var inputDir = workingDir + "/input";
      fs.readdir(inputDir, function (err, files) {
        if (err) {
          debug("cannot read workspace", inputDir);
          return callback("Das Arbeitsverzeichnis konnte nicht gelesen werden! Bitte wende dich an Cid.");
        }

        unlinkFiles(inputDir, files);
      });

      var outputDir = workingDir + "/output";
      fs.readdir(outputDir, function (err, files) {
        if (err) {
          debug("cannot read workspace", outputDir);
          return callback("Das Arbeitsverzeichnis konnte nicht gelesen werden! Bitte wende dich an Cid.");
        }

        unlinkFiles(outputDir, files);
      });
    });
  }
};

/**
 * The websocket events.
 */
var clients = [];

wss.on("connection", function (ws) {
  debug("new websocket connection");
  clients.push(ws);
  debug("clients total", clients.length);

  ws.on("close", function (code, message) {
    debug("websocket disconnected", code, message);
    var index = clients.indexOf(ws);
    if (index > -1) {
      clients.splice(index, 1);
      debug("clients total", clients.length);
    }
  });
});

wss.on("error", function (err) {
  debug("error", err);
});

wamps.on("call", function (procUri, args, callback) {
  if (!procs[procUri]) {
    return callback("Unknown procedure: '" + procUri + "'");
  }
  procs[procUri](args, callback);
});
